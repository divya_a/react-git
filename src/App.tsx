import React from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import Settings from "./components/settings";
import Home from "./components/Home";
import { Button } from "antd";
import Calendar from "./components/Calendar";
import { Menu, Icon } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
//import Message from "./Message";
//import Axios from "axios";

function App() {
  return (
    <Router>
      <div
        style={{
          //see some additional required styles in index.css
          display: "flex",
          flexDirection: "column",
          height: "100%"
        }}
      >
        {/* this is the currently selected view */}
        <div
          style={{
            flexGrow: 1,
            overflowX: "hidden",
            overflowY: "auto"
          }}
        >
          <Switch>
            <Route path="/home" component={Home} />
            <Route path="/settings" component={Settings} />
            <Route path="/calendar" component={Calendar} />
            <Redirect from="/" to="/home/" />
          </Switch>
        </div>

        <Menu
          icon="labeled"
          borderless
          widths={3}
          style={{
            flexShrink: 0, //don't allow flexbox to shrink it
            borderRadius: 0, //clear semantic-ui style
            margin: 0 //clear semantic-ui style
          }}
        >
          <Menu.Item as={NavLink} to="/home/">
            <Icon name="home" />
            Home page
          </Menu.Item>
          <Menu.Item as={NavLink} to="/calendar/">
            <Icon name="calendar" />
            Calendar
          </Menu.Item>
          <Menu.Item as={NavLink} to="/settings/">
            <Icon name="settings" />
            Settings
          </Menu.Item>
        </Menu>
      </div>
    </Router>
  );
}

export default App;

// const initialState = {
//   name: "Manny",
//   message: "Typescript is cool!"
// };
// type State = Readonly<typeof initialState>;

// class App extends Component<any, State> {
//   readonly state: State = initialState;
//   render() {
//     return (
//       <div className="App">
//         <p>
//           <Message name={this.state.name} message={this.state.message} />
//         </p>
//       </div>
//     );
//   }
// }

// const App = () => {
//   const [data, setData] = useState<any>();

//   const getDataFromFDA = () => {
//     Axios.get(
//       "https://api.fda.gov/animalandveterinary/event.json?search=original_receive_date:[20040101+TO+20161107]&limit=3"
//     ).then((response: any) => {
//       setData(response.data);
//     });
//   };

//   useEffect(() => {
//     getDataFromFDA();
//   }, []);
//   return <div className="App">{JSON.stringify(data)}</div>;
// };

//export default App;
