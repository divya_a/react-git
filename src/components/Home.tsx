import React from "react";
import { Button } from "antd";
//import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Settings from "./settings";
import Calendar from "./Calendar";
import { Card, Avatar } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
  AlignCenterOutlined
} from "@ant-design/icons";
import Meta from "antd/lib/card/Meta";
import { Menu } from "semantic-ui-react";

//const { Meta } = Card;
function Home() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        height: "100%"
      }}
    >
      {/* this header is fixed to the top */}
      <Menu
        borderless
        style={{
          flexShrink: 0, //don't allow flexbox to shrink it
          borderRadius: 0, //clear semantic-ui style
          margin: 0 //clear semantic-ui style
        }}
      >
        <Menu.Item header>Newsfeed</Menu.Item>
      </Menu>
      {/* this section fills the rest of the page */}
      <div
        style={{
          flexGrow: 1,
          overflowX: "hidden",
          overflowY: "auto"
        }}
      >
        <div>
          <Card
            style={{ width: 300, marginTop: 20, marginLeft: 30 }}
            cover={
              <img
                alt="example"
                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
              />
            }
            actions={[
              <SettingOutlined key="setting" />,
              <EditOutlined key="edit" />,
              <EllipsisOutlined key="ellipsis" />
            ]}
          >
            <Meta
              avatar={
                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              }
              title="Divya"
              description="Love to see you here!"
            />
          </Card>

          {/* <button onClick={() => push("/calendar")}>Calendar</button> */}
        </div>
      </div>
    </div>
  );
}

export default Home;
